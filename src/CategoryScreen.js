'use strict';

import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Butto, TouchableHighlight } from 'react-native';

export default class CategoryScreen extends Component {
  static navigationOptions = {
    title: 'Category',
  };

  constructor(props) {
    super(props);
    const {navigation} = this.props;
    const categories = navigation.getParam('categories', []);
    this.state = {categories: categories}
  }

  render() {
    console.log('CategoryScreen.render');
    return this.CategoryList();
  }

  CategoryList() {

    return (
        <View style={styles.container}>
          {this.state.categories.map((category) => {
            return (
              <TouchableHighlight onPress={() => this._onGoToCatalogue(category)} underlayColor="green" key={category.id}>
                <Text style={styles.description}>{category.title}</Text>
              </TouchableHighlight>
            )
          })}
         </View>
     );
  }

  _onGoToCatalogue(category) {
    this.props.navigation.navigate('Catalogue', {path: category.path, categories: this.state.categories});
  }

}

const styles = StyleSheet.create({
  description: {
    fontSize: 18,
    textAlign: 'center',
    color: '#656565',
    marginTop: 65
  },
  container: {
    padding: 30,
    marginTop: 65,
    alignItems: 'center'
  },
  image: {
    width: 160,
    height: 170
  }
});
