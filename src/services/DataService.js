export const fetchData = (endpoint) => {
  const URL = `http://10.188.88.161:3000/${endpoint}`;
  return fetch(URL)
    .then(response => {
      if (response.status === 200) {
        return response.json();
      } else {
        throw new Error('[fetchData]: Something went wrong on api server! - ' + endpoint);
      }
    })
    .catch(error => {
      console.error(error);
    });
}

export const fetchDataByEndpointAttributeValue = (endpoint, attribute, value) => {
  const URL = `http://10.188.88.161:3000/${endpoint}?` + attribute + "=" + value;
  return fetch(URL)
    .then(response => {
      if (response.status === 200) {
        return response.json();
      } else {
        throw new Error('[fetchData]: Something went wrong on api server! - ' + endpoint);
      }
    })
    .catch(error => {
      console.error(error);
    });
}
