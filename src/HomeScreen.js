'use strict'

import React, {Component} from 'react'
import { View, Text, StyleSheet, Image, Button } from 'react-native'
import { fetchData } from './services/DataService'
import CategoryScreen from './CategoryScreen'
import LogoTile from './components/LogoTile'


export default class HomeScreen extends Component {
  static navigationOptions = {
    headerLeft: <LogoTile />
  };

  constructor(props) {
    super(props);
    this.state = {
      connected: false
    };
  }

  _onDeviceConnected = (event) => {
    console.log('_onDeviceConnected');
    this.setState({ connected: true });
    const {navigate} = this.props.navigation;
    console.log("Navigate: " + navigate);
    fetchData('services')
      .then((res) => {
              if (res.message === 'Not Found') {
                this.setState({
                    error: 'No services found'
                });
              } else {
                navigate('Category', {categories: res});
                this.setState({
                  error: false,
                  connected: true
                })
              }
        });
  };

  render() {
    console.log('CategoryScreen.render');
    return this.ConnectInfo();
  }

  ConnectInfo() {
    console.log('CategoryScreen.render.notConnected');
    return(
      <View style={styles.container}>
         <Text style={styles.description}>Please, connect to the nearest street lamp</Text>
         <Image source={require('./../Resources/lamp.jpg')} style={styles.image}/>
         <Button
            onPress={this._onDeviceConnected}
            title="Connect simulation"
            color="#841584"
          />
       </View>
     );
  }

}

const styles = StyleSheet.create({
  description: {
    fontSize: 18,
    textAlign: 'center',
    color: '#656565',
    marginTop: 65
  },
  container: {
    padding: 30,
    marginTop: 65,
    alignItems: 'center'
  },
  image: {
    width: 160,
    height: 170
  }
});
