'use strict';
import React, {Component} from 'react';
import { View, Text, StyleSheet} from 'react-native';

export default class DetailScreen extends Component {
  static navigationOptions = {
    title: 'Details',
  };

  constructor(props) {
    super(props);
  }

  DetailView() {
    const {navigation} = this.props;
    const option = navigation.getParam('option', null);

    return (
        <View style={styles.container}>
            <Text style={styles.description}>{option.title}</Text>
            <Text style={styles.description}>{option.subtitle}</Text>
            <Text style={styles.description}>{option.description}</Text>
            <Text style={styles.description}>{option.info.coordinates.longitude} {option.info.coordinates.latitude}</Text>
            <Text style={styles.description}>{option.info.address.street}</Text>
            <Text style={styles.description}>{option.info.website}</Text>
         </View>
     );
  }

  render() {
    console.log('CatalogueScreen.render');
    return this.DetailView();
  }

}

const styles = StyleSheet.create({
  container: {
    padding: 30,
    marginTop: 65,
    alignItems: 'center'
  },
  image: {
    width: 160,
    height: 170
  },
  description: {
    fontSize: 10,
    textAlign: 'center',
    color: '#656565',
    marginTop: 5
  }
});
