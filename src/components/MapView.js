'use strict'
import React from 'react'
import Map from './components/Map'

export default class MapView extends React.Component {

  render() {
    return (
        <Map startLatitude={43.7} startLongitude={7.25} markers={this.state.markers} onGoToDetails={this._goToDetail} />
     );
  }

  _goToDetail = (marker) => {
    this.props.navigation.navigate('Details', {option: marker});
  }

}
