'use strict'

import React, {Component} from 'react'
import {View, Image, Text} from 'react-native'
import { Card } from 'react-native-elements';

export default class ServiceTile extends Component {

  render() {
    return (
      <View style={{width: 230, height: 230, borderWidth: 0.5, borderColor: 'grey', borderRadius: 15}}>
        <Card
          containerStyle={{ borderRadius: 10 }}
          imageStyle={{
            borderWidth: 1,
            borderColor: '#F44336',
            borderRadius: 10,
            overflow: 'hidden'
          }}
          image={require('../../Resources/lamp.jpg')}
        >
          <Text style={{ marginBottom: 10 }}>
            {this.props.service.title}
          </Text>
        </Card>
      </View>
    )
  }

}
