'use strict'
import React, {Component} from 'react'
import { Image } from 'react-native';

export default class LogoTile extends Component {
  render() {
    return (
      <Image
        source={require('../../Resources/smartcity.png')}
        style={{ width: 45, height: 45 }}
      />
    );
  }
}
